mod board;
mod r#move;
mod solver;

use std::fs;

use anyhow::Error;
use argh::FromArgs;

use crate::board::Board;
use crate::solver::Solver;
use crate::r#move::Move;

#[derive(FromArgs)]
/// Solve a peg solitaire board
struct Args {
    #[argh(positional)]
    inputfile: String,
}

fn print_solution(solution: &[Move]) {
    for mov in solution {
        println!("{}", mov);
    }
}

fn main() -> Result<(), Error> {
    let args: Args = argh::from_env();

    let contents = fs::read(&args.inputfile)?;
    let contents = String::from_utf8(contents)?;

    let board: Board = contents.parse()?;
    let solver = Solver::new(board);

    match solver.solve() {
        None => { println!("No solution found") }
        Some(solution) => { print_solution(&solution) }
    }

    Ok(())
}
