use std::hash::Hasher;

use anyhow::{bail, Error};
use array2d::Array2D;
use ahash::AHasher;

use crate::r#move::{Direction, Move};

#[derive(Clone, PartialEq, Eq, Debug)]
enum Space {
    Full,
    Empty,
    Invalid,
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Board {
    spaces: Array2D<Space>,
}

pub type BoardHash = u64;

#[derive(Clone, Copy, Debug)]
pub enum Rotation {
    Right0,
    Right90,
    Right180,
    Right270,
}

impl Space {
    fn from_char(c: char) -> Result<Self, Error> {
        use Space::*;

        let space = match c {
            ' ' => Invalid,
            'o' => Full,
            '.' => Empty,
            _ => bail!("Invalid space value: {}", c),
        };

        Ok(space)
    }
}

impl Board {
    pub fn apply(&mut self, mov: &Move) {
        let start = self.spaces.get_mut(mov.row, mov.column).expect("Invalid move start");
        assert!(*start == Space::Full);
        *start = Space::Empty;

        let (jump_row, jump_col) = mov.shift_coords(1).expect("Invalid jump coord");
        let jump = self.spaces.get_mut(jump_row, jump_col).expect("Invalid move jump");
        assert!(*jump == Space::Full);
        *jump = Space::Empty;

        let (land_row, land_col) = mov.shift_coords(2).expect("Invalid land coord");
        let land = self.spaces.get_mut(land_row, land_col).expect("Invalid move land");
        assert!(*land == Space::Empty);
        *land = Space::Full;
    }

    pub fn unapply(&mut self, mov: &Move) {
        let start = self.spaces.get_mut(mov.row, mov.column).expect("Invalid move start");
        assert!(*start == Space::Empty);
        *start = Space::Full;

        let (jump_row, jump_col) = mov.shift_coords(1).expect("Invalid jump coord");
        let jump = self.spaces.get_mut(jump_row, jump_col).expect("Invalid move jump");
        assert!(*jump == Space::Empty);
        *jump = Space::Full;

        let (land_row, land_col) = mov.shift_coords(2).expect("Invalid land coord");
        let land = self.spaces.get_mut(land_row, land_col).expect("Invalid move land");
        assert!(*land == Space::Full);
        *land = Space::Empty;
    }

    pub fn possible_moves(&self) -> impl Iterator<Item=Move> + '_ {
        (0..self.spaces.num_rows()).flat_map(
            |row| (0..self.spaces.num_columns()).flat_map(
                move |column| [
                    Move{ row, column, direction: Direction::Right},
                    Move{ row, column, direction: Direction::Down},
                    Move{ row, column, direction: Direction::Left},
                    Move{ row, column, direction: Direction::Up},
                ].into_iter()))
            .filter_map(|mov| {
                let start = self.spaces.get(mov.row, mov.column)?;
                if *start != Space::Full {
                    return None;
                }

                let (jump_row, jump_col) = mov.shift_coords(1)?;
                let jump = self.spaces.get(jump_row, jump_col)?;
                if *jump != Space::Full {
                    return None;
                }

                let (land_row, land_col) = mov.shift_coords(2)?;
                let land = self.spaces.get(land_row, land_col)?;
                if *land != Space::Empty {
                    return None;
                }

                Some(mov)
            })
    }

    pub fn hash(&self) -> BoardHash {
        let mut hasher = AHasher::default();
        for space in self.spaces.elements_row_major_iter() {
            hasher.write(
                match *space {
                    Space::Invalid => b" ",
                    Space::Empty => b".",
                    Space::Full => b"o",
                }
            );
        }

        hasher.finish()
    }

    pub fn pegs_remaining(&self) -> usize {
        self.spaces.elements_row_major_iter().filter(|space| **space == Space::Full).count()
    }

    pub fn is_solved(&self) -> bool {
        self.pegs_remaining() == 1
    }

    pub fn transformed(&self, rotation: Rotation, flip: bool) -> Option<Self> {
        let num_rows = self.spaces.num_rows();
        let num_columns = self.spaces.num_columns();

        let elements: Box<dyn Iterator<Item=Space>> = match rotation {
            Rotation::Right0 => {
                Box::new(self.spaces.as_rows().into_iter().map(
                    |row| {
                        let row_iter: Box<dyn Iterator<Item=_>> = if flip {
                            Box::new(row.into_iter().rev())
                        } else {
                            Box::new(row.into_iter())
                        };
                        row_iter
                    }
                ).flatten())
            }
            Rotation::Right90 => {
                if num_rows != num_columns {
                    return None;
                }

                Box::new(self.spaces.as_columns().into_iter().map(
                    |row| {
                        let row_iter: Box<dyn Iterator<Item=_>> = if flip {
                            Box::new(row.into_iter())
                        } else {
                            Box::new(row.into_iter().rev())
                        };
                        row_iter
                    }
                ).flatten())
            }
            Rotation::Right180 => {
                Box::new(self.spaces.as_rows().into_iter().map(
                    |row| {
                        let row_iter: Box<dyn Iterator<Item=_>> = if flip {
                            Box::new(row.into_iter())
                        } else {
                            Box::new(row.into_iter().rev())
                        };
                        row_iter
                    }
                ).rev().flatten())
            }
            Rotation::Right270 =>  {
                if num_rows != num_columns {
                    return None;
                }

                Box::new(self.spaces.as_columns().into_iter().map(
                    |row| {
                        let row_iter: Box<dyn Iterator<Item=_>> = if flip {
                            Box::new(row.into_iter().rev())
                        } else {
                            Box::new(row.into_iter())
                        };
                        row_iter
                    }
                ).rev().flatten())
            }
        };
        let spaces = Array2D::from_iter_row_major(elements, num_rows, num_columns);
        Some(Board { spaces })
    }
}

impl std::str::FromStr for Board {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parsed = s
            .lines()
            .map(|line| {
                line.chars()
                    .map(Space::from_char)
                    .collect::<Result<Vec<_>, _>>()
            })
            .collect::<Result<Vec<_>, _>>()?;

        if let Some(first_row) = parsed.get(0) {
            let row_len = first_row.len();
            for row in parsed.iter() {
                if row.len() != row_len {
                    bail!("Board has mismatched row lengths");
                }
            }
        } else {
            bail!("Input board was empty");
        }

        let spaces = Array2D::from_rows(&parsed);

        Ok(Board { spaces })
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use Space::*;

    #[test]
    fn test_from_str_simple() {
        let board: Board = "ooo
ooo
oo."
            .parse()
            .expect("Parse failed");

        let expected = Array2D::from_row_major(
            &[Full, Full, Full, Full, Full, Full, Full, Full, Empty],
            3,
            3,
        );
        assert!(board.spaces == expected);
    }

    #[test]
    fn test_from_str_with_invalid() {
        let board: Board = " oo
 oo
oo.
 oo"
            .parse()
            .expect("Parse failed");

        let expected = Array2D::from_row_major(
            &[
                Invalid, Full, Full, Invalid, Full, Full, Full, Full, Empty, Invalid, Full, Full,
            ],
            4,
            3,
        );
        assert!(board.spaces == expected);
    }

    #[test]
    fn test_from_str_with_mismatched_rows() {
        "ooo
oooo
ooo"
            .parse::<Board>()
            .expect_err("Should have returned error");
    }

    #[test]
    fn test_possible_moves_none() {
        let board: Board = "ooo
ooo
ooo"
            .parse()
            .expect("Parse failed");
        let possible_moves: Vec<_> = board.possible_moves().collect();

        let expected = vec![];
        assert!(possible_moves == expected);
    }

    #[test]
    fn test_possible_moves_some() {
        let board: Board = "ooo
ooo
oo."
            .parse()
            .expect("Parse failed");
        let possible_moves: Vec<_> = board.possible_moves().collect();

        let expected = vec![
            Move { row: 0, column: 2, direction: Direction::Down },
            Move { row: 2, column: 0, direction: Direction::Right },
        ];
        assert!(possible_moves == expected);
    }

    #[test]
    fn test_transformed_square() {
        let initial_board: Board = "oo.
.o.
o.."
            .parse()
            .expect("Parse failed");

        let tests = [
            (Rotation::Right0, false, Some("oo.
.o.
o..")),
            (Rotation::Right90, false, Some("o.o
.oo
...")),
            (Rotation::Right180, false, Some("..o
.o.
.oo")),
            (Rotation::Right270, false, Some("...
oo.
o.o")),
            (Rotation::Right0, true, Some(".oo
.o.
..o")),
            (Rotation::Right90, true, Some("o.o
oo.
...")),
            (Rotation::Right180, true, Some("o..
.o.
oo.")),
            (Rotation::Right270, true, Some("...
.oo
o.o")),
        ];

        for (rotation, flip, expected) in tests {
            let transformed = initial_board.transformed(rotation, flip);
            let expected = expected.map(|s| s.parse::<Board>().unwrap());
            assert!(transformed == expected, "Mismatch on case rotation: {:?}, flip: {:?}\nexpected: {:?}\ngot: {:?}", rotation, flip, expected, transformed);
        }
    }

    #[test]
    fn test_transformed_rectangle() {
        let initial_board: Board = "oo.
.o."
            .parse()
            .expect("Parse failed");

        let tests = [
            (Rotation::Right0, false, Some("oo.
.o.")),
            (Rotation::Right90, false, None),
            (Rotation::Right180, false, Some(".o.
.oo")),
            (Rotation::Right270, false, None),
            (Rotation::Right0, true, Some(".oo
.o.")),
            (Rotation::Right90, true, None),
            (Rotation::Right180, true, Some(".o.
oo.")),
            (Rotation::Right270, true, None),
        ];

        for (rotation, flip, expected) in tests {
            let transformed = initial_board.transformed(rotation, flip);
            let expected = expected.map(|s| s.parse::<Board>().unwrap());
            assert!(transformed == expected, "Mismatch on case rotation: {:?}, flip: {:?}\nexpected: {:?}\ngot: {:?}", rotation, flip, expected, transformed);
        }
    }
}
