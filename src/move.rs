#[derive(Debug, PartialEq, Eq)]
pub enum Direction {
    Right,
    Down,
    Left,
    Up,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Move {
    pub row: usize,
    pub column: usize,
    pub direction: Direction,
}

impl Move {
    pub fn shift_coords(&self, amount: usize) -> Option<(usize, usize)> {
        let mut row = self.row;
        let mut col = self.column;

        match &self.direction {
            Direction::Right => {col += amount;}
            Direction::Down => {row += amount;}
            Direction::Left => {
                if col < amount {
                    return None;
                }
                col -= amount;
            }
            Direction::Up => {
                if row < amount {
                    return None;
                }
                row -= amount;
            }
        };

        Some((row, col))
    }
}

impl std::fmt::Display for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> { 
        write!(f, "({}, {}) - {:?}", self.row, self.column, self.direction)?;
        Ok(())
    }
}
