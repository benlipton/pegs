use std::collections::HashSet;

use crate::board::{Board, BoardHash, Rotation};
use crate::r#move::Move;

pub struct Solver {
    board: Board,
    seen: HashSet<BoardHash>,
    best_count: usize,
    total_iterations: usize,
    skipped_iterations: usize,
}

impl Solver {
    pub fn new(board: Board) -> Self {
        let pegs_remaining = board.pegs_remaining();

        Self {
            board: board,
            seen: HashSet::new(),
            best_count: pegs_remaining,
            total_iterations: 0,
            skipped_iterations: 0,
        }
    }

    pub fn solve(mut self) -> Option<Vec<Move>> {
        let solution = self.solve_ref();

        self.print_stats();

        match solution {
            None => None,
            Some(solution) => Some(solution.into_iter().rev().collect()),
        }
    }

    fn is_seen_board(&self) -> bool {
        let board_hash = self.board.hash();

        self.seen.contains(&board_hash)
    }

    fn store_board(&mut self) {
        for flip in [true, false] {
            for rotation in [Rotation::Right0, Rotation::Right90, Rotation::Right180, Rotation::Right270] {
                if let Some(transformed) = self.board.transformed(rotation, flip) {
                    let board_hash = transformed.hash();
                    self.seen.insert(board_hash);
                }
            }
        }
    }

    fn solve_ref(&mut self) -> Option<Vec<Move>> {
        let moves: Vec<_> = self.board.possible_moves().collect();
        for next_move in moves {
            self.board.apply(&next_move);

            // Update/print statistics
            let pegs_remaining = self.board.pegs_remaining();
            if pegs_remaining < self.best_count {
                self.best_count = pegs_remaining;
                self.print_stats();
            }
            self.total_iterations += 1;

            if self.total_iterations % 1000000 == 0 {
                self.print_stats();
            }

            if !self.is_seen_board() {
                self.store_board();

                if self.board.is_solved() {
                    let mut solution = Vec::new();
                    solution.push(next_move);
                    return Some(solution);
                }

                if let Some(mut solution) = self.solve_ref() {
                    solution.push(next_move);
                    return Some(solution);
                }
            } else {
                self.skipped_iterations += 1;
            }

            self.board.unapply(&next_move);
        }

        None
    }

    fn print_stats(&self) {
        let pct_skipped = self.skipped_iterations as f64 / (self.total_iterations+1) as f64 * 100.0;

        println!("iterations: {} | skipped: {} ({:.1}%) | best count: {}", self.total_iterations, self.skipped_iterations, pct_skipped, self.best_count);
    }
}
